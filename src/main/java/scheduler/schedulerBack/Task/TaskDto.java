package scheduler.schedulerBack.Task;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class TaskDto {

    private String title;
    private String text;
    private TaskStatus taskStatus;
    private String category;

}
