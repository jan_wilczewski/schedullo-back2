package scheduler.schedulerBack.Task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import scheduler.schedulerBack.Category.Category;
import scheduler.schedulerBack.Category.CategoryRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TaskService {

    @Autowired
    TaskRepository taskRepository;
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    private Task taskModel;
    @Autowired
    private TaskDto taskDto;


    private List<Task> findAllTasks() {
        return taskRepository.findAll();
    }

    List<TaskDto> provideAllTasksDto() {

        List<Task> tasks = findAllTasks();
        List<TaskDto> tasksDto = new ArrayList<>();

//        for (Task task: tasks) {
//            tasksDto.add(taskModel.transformTaskToTaskDto(task));
//        }
        return tasksDto;
    }


    Task insertNewTask(Task task) {
        taskRepository.save(task);
        return task;
    }

    TaskDto getTaskById(Long id) {
        Optional<Task> taskOptional = taskRepository.findById(id);
        Optional<TaskDto> taskDtoOptional = taskOptional.map(Task::transformTaskToTaskDto);
//        if (taskOptional.isPresent()) {
//            taskDto = taskModel.transformTaskToTaskDto(taskOptional.get());
//            return taskDto;
//        }
        return null;
    }


    Task provideTaskToEdit(Long taskId) {
        Optional<Task> taskOptional = taskRepository.findById(taskId);

        if (!taskOptional.isPresent()) {
            return null;
        }

        Task taskToEdit = taskOptional.get();
        return taskToEdit;
    }

    Category provideCategoryToEdit(Long categoryId) {
        Optional<Category> categoryOptional = categoryRepository.findById(categoryId);

        if (!categoryOptional.isPresent()) {
            return null;
        }
        Category categoryToEdit = categoryOptional.get();
        return categoryToEdit;
    }


    void editTask(Task task) {
        taskRepository.save(task);
    }
}
