package scheduler.schedulerBack.Task;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import scheduler.schedulerBack.Category.Category;

import javax.persistence.*;

@Entity
@Table(name = "task")
@Data
@NoArgsConstructor
@Component
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String text;

    @Enumerated(EnumType.STRING)
    private TaskStatus taskStatus;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CATEGORY_ID")
    private Category category;

    public Task(String title) {
        this.title = title;
    }

    public Task(String title, String text, TaskStatus taskStatus) {
        this.title = title;
        this.text = text;
        this.taskStatus = taskStatus;
    }

    public Task(String title, String text, TaskStatus taskStatus, Category category) {
        this.title = title;
        this.text = text;
        this.taskStatus = taskStatus;
        this.category = category;
    }

    public TaskDto transformTaskToTaskDto() {
        TaskDto taskDto = new TaskDto();
        taskDto.setTitle(this.getTitle());
        taskDto.setText(this.getText());
        taskDto.setTaskStatus(this.getTaskStatus());
        taskDto.setCategory(this.getCategory().getName());
        return taskDto;
    }
}
