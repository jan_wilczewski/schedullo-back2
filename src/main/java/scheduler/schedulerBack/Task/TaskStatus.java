package scheduler.schedulerBack.Task;

public enum TaskStatus {
    TODO, SUSPENDED, IN_PROGRESS, SENT_FOR_APPROVAL, REVISION, ACCEPTED
}
