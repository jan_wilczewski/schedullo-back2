package scheduler.schedulerBack.Task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import scheduler.schedulerBack.Category.Category;
import scheduler.schedulerBack.Category.CategoryRepository;
import scheduler.schedulerBack.ExceptionHandler.TaskNotFoundException;

import javax.xml.ws.http.HTTPException;
import java.util.Optional;

@Service
public class TaskServiceHttp {

    @Autowired
    private TaskService taskService;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private TaskRepository taskRepository;


    ResponseEntity<TaskDto> getTaskResponseEntityById(Long id) {

        Optional<TaskDto> taskDto = Optional.ofNullable(taskService.getTaskById(id));

        ResponseEntity<TaskDto> response = taskDto.map(taskDto1 -> ResponseEntity
                .ok()
                .body(taskDto1)).orElseGet(() -> ResponseEntity.notFound().build());

        return response;
    }


    ResponseEntity<Void> updateCategory(Long taskId, Long categoryId) {

        Task taskToEdit = taskService.provideTaskToEdit(taskId);
        Category categoryToEdit = taskService.provideCategoryToEdit(categoryId);

        if (categoryToEdit != null) {
            taskToEdit.setCategory(categoryToEdit);

            categoryRepository.save(categoryToEdit);
            taskRepository.save(taskToEdit);

            return ResponseEntity
                    .ok()
                    .build();
        }

        return ResponseEntity
                .badRequest()
                .build();
    }

    ResponseEntity<Void> deleteTask(Long id) {
        try {
            taskRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (HTTPException e) {
            return new ResponseEntity<>(HttpStatus.valueOf(e.getStatusCode()));
        }
    }
}
