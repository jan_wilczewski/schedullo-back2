package scheduler.schedulerBack.Task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/tasks")
public class TaskController {

    @Autowired
    private TaskService taskService;
    @Autowired
    private TaskServiceHttp taskServiceHttp;



    @PostMapping(value = "/insert", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Task insert(@RequestBody Task task) {
        return taskService.insertNewTask(task);
    }

    @PutMapping(value = "/edit")
    public void editTask(@Valid @RequestBody Task task) {
        taskService.editTask(task);
    }


    @GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<TaskDto> get() {
        return taskService.provideAllTasksDto();
    }


    @GetMapping(value = "/getid/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TaskDto> getById(@PathVariable("id") Long id) {
        return taskServiceHttp.getTaskResponseEntityById(id);
    }

    @DeleteMapping(value = "/delete/{id}")
    @Transactional
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        return taskServiceHttp.deleteTask(id);
    }


    @PutMapping(value = "/update/{taskId}/{categoryId}")
    @Transactional
    public ResponseEntity<Void> updateCategory(@PathVariable("taskId") Long taskId, @PathVariable("categoryId") Long categoryId) {
        return taskServiceHttp.updateCategory(taskId, categoryId);
    }


}
