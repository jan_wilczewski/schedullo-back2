package scheduler.schedulerBack.Category;

import lombok.Data;
import scheduler.schedulerBack.Task.Task;

import java.util.Set;

@Data
public class CategoryDto {

    private String name;
    private Set<Task> tasks;

}
