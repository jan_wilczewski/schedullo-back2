package scheduler.schedulerBack.Category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private CategoryServiceHttp categoryServiceHttp;

    @GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CategoryDto>> get() {
        List<Category> categories = categoryService.provideAllCategories();
        List<CategoryDto> categoriesDto = categoryService.provideAllCategoriesDto(categories);
        return categoryServiceHttp.provideResponseEntityCategories(categoriesDto);
    }
}
