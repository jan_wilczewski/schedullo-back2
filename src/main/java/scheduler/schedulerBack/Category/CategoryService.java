package scheduler.schedulerBack.Category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private Category category;

    List<Category> provideAllCategories() {
        return categoryRepository.findAll();
    }

    List<CategoryDto> provideAllCategoriesDto(List<Category> categories) {
        List<CategoryDto> categoriesDto = new ArrayList<>();
        for (Category cat: categories) {
            categoriesDto.add(category.transformCategoryToCategoryDto(cat));
        }
        return categoriesDto;
    }

}



