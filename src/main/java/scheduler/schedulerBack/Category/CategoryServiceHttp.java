package scheduler.schedulerBack.Category;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceHttp {

    public ResponseEntity<List<CategoryDto>> provideResponseEntityCategories(List<CategoryDto> categoriesDto) {
        return ResponseEntity
                .ok()
                .body(categoriesDto);
    }

}
