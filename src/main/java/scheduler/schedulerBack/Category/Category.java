package scheduler.schedulerBack.Category;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.stereotype.Component;
import scheduler.schedulerBack.Task.Task;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "category")
@Getter
@Setter
@NoArgsConstructor
@Component
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @Fetch(FetchMode.SELECT)
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "CATEGORY_ID")
    @JsonIgnore
    private Set<Task> tasks = new HashSet<>();

    private String expensiveField;


    public Category(Long id) {
        this.id = id;
    }
    public Category(String name) {
        this.name = name;
    }
    public Category(String name, Set<Task> tasks) {
        this.name = name;
        this.tasks = tasks;
    }

    public CategoryDto transformCategoryToCategoryDto(Category category) {
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setName(category.getName());
        categoryDto.setTasks(category.getTasks());
        return categoryDto;
    }

    public String getExpensive() {
        if(expensiveField == null){
            expensiveField = performrExpensiveStuff();
        }
        return expensiveField;
    }
}
