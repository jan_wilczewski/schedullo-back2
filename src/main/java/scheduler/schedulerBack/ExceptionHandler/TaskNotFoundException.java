package scheduler.schedulerBack.ExceptionHandler;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="Task Not Found") //404
public class TaskNotFoundException extends Exception {

    private static final long serialVersionUID = -3332292346834265371L;
    private final Long taskId;

    public TaskNotFoundException(Long id) {
        this.taskId = id;
    }

    public Long getTaskId() {
        return taskId;
    }
}
