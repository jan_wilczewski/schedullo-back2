package scheduler.schedulerBack.ExceptionHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;


@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @Autowired
    private MessageSource messageSource;

    @ExceptionHandler(TaskNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ResponseBody
    public ErrorInfo taskNotFound(HttpServletRequest request, TaskNotFoundException exception) {
        Locale locale = LocaleContextHolder.getLocale();
        String errorMessage = messageSource.getMessage("error.no.task.id", null, locale);

        errorMessage += exception.getTaskId();
        String errorURL = request.getRequestURL().toString();

        return new ErrorInfo(errorURL, errorMessage);
    }


//    @ExceptionHandler(TaskNotFoundException.class)
//    public ResponseEntity<TaskNotFoundException> handleTaskNotFoundException(HttpServletRequest request, Exception ex, Long id){
//        logger.info("TaskNotFoundException Occured:: URL="+request.getRequestURL());
//        TaskNotFoundException exception = new TaskNotFoundException(id, taskId);
//
//        return new ResponseEntity<>(exception, HttpStatus.OK);
//    }


//    https://www.journaldev.com/2651/spring-mvc-exception-handling-controlleradvice-exceptionhandler-handlerexceptionresolver
}
