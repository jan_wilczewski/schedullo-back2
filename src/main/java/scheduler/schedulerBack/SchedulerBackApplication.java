package scheduler.schedulerBack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = "scheduler.schedulerBack")
@EnableJpaRepositories("scheduler.schedulerBack")
@EntityScan("scheduler.schedulerBack")
public class SchedulerBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(SchedulerBackApplication.class, args);
	}
}
